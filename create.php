<?php
	require_once('lib/app.php');
	 
?>
<!DOCTYPE html>
<html>
<head>
	<title>create</title>
</head>
<body>
<form action="add.php" method="post">
	<fieldset>
		<legend>Gender Info</legend>
		<div>
			<label for="txtFullName">Full name</label>
			<input type="text" name="full_name" id="txtFullName"value="">
		</div>

		<div>
			<label for="city">Select City</label>
			<select name="city" id="city">
				<option disabled="disabled" selected="selected">Select A city</option>
				<option value="Chittagong">Chittagong</option>
				<option value="Dhaka">Dhaka</option>
				<option value="Sylhet">Sylhet</option>
				<option value="Rajshahi">Rajshahi</option>
			</select>
		</div>

		<input type="submit" value="Save Info">
	</fieldset>
</form>

</body>
</html>